module.exports = class HelperArray {
  static getIds_ (arr) {
    return arr.map(item => {
      return item._id;
    });
  }

  static getIds (arr) {
    return arr.map(item => {
      return item.id;
    });
  }

  static objectToArray (object) {
    return Object.keys(object).map((k) => object[k]);
  }

  static indexedObject (array, propsForIndexed, type) {
    let object = {};
    array.forEach(item => {
      if (type === 'pg') {
        item = item.dataValues ? item.dataValues : item;
      }
      let indexed = '';
      propsForIndexed.forEach(prop => {
        indexed += item[prop];
      });
      object[indexed] = item;
    });
    return object;
  }

  static compareArraysAndGetListsUpdateAndCreate (arrayCurrent, arrayForCheck, propsForIndexed, type) {
    let object = this.indexedObject(arrayCurrent, propsForIndexed, type);
    let result = {
      updates: [],
      creates: []
    };

    arrayForCheck.forEach(item => {
      let changed = null;
      // fix by type
      if (type === 'pg') {
        item = item.dataValues ? item.dataValues : item;
      }
      let indexed = '';
      // create indexed by prop
      propsForIndexed.forEach(prop => {
        indexed += item[prop];
      });
      // check not exist
      if (!object[indexed]) {
        return result.creates.push(item);
      }
      // check for update
      for (var key in item) {
        let value = item[key];
        if (object[indexed][key].toString().trim() !== value.trim()) {
          changed = true;
        }
      }
       // add if its update
      if (changed) {
        result.updates.push(item);
      }
    });
    return result;
  }

  static updateAndCreate (updates, funcUpdate, creates, funcCreate) {
    let update = () => {
      return Promise((resolve, reject) => {
        if (updates) {
          return funcUpdate(updates);
        }
        return resolve();
      });
    };

    let create = () => {
      return Promise((resolve, reject) => {
        if (updates) {
          return funcCreate(creates);
        }
        return resolve();
      });
    };

    return update().then(() => {
      return create();
    });
  }
};
