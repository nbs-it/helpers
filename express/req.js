const request = require('request');

module.exports.action = function (method, url, options, body) {
  if (!options) {
    options = {
      headers: {

      }
    };
  }
  options.method = method;
  options.url = url;
  if (method === 'POST') {
    options.body = body;
  }

  return new Promise((resolve, reject) => {
    request(options,
        (error, response, resBody) => {
          if (error) {
            return reject(error);
          }
          return resolve(resBody);
        });
  });
};
