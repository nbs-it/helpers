/*
  extending res for resolve  success and error data
*/
module.exports = (req, res, next) => {
  res.error = (errorEvent) => {
    return res.json({
      status: 'error',
      err: errorEvent.toString()
    });
  };

  res.success = (data) => {
    return res.json({
      status: 'success',
      data: data
    });
  };

  res.resolve = (promiseObject) => {
    promiseObject
    .then(res.success)
    .catch(res.error);
  };

  next();
};
