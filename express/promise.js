module.exports = class HelperPromise {
  /**
   * TODO...
   *
   * @static
   * @param {object array} arrayValues - an array of values for send to service
   * @param {object array} service - an service then hold promise func
   * @param {object array} functionService - name of promise func then holding in service
   * @param {object array} numberLoop - number of promises for sending together each time
   * @param {object array} argumentIsArray - if argument is array we sending to function in service all array like arguments by using apply
   * @returns TODO...
   * @memberof HelperPromise
   */
  static arrayPromise (arrayValues, service, functionService, numberLoop, arrayValuesItemsIsNotObject) {
    let array = arrayValues.slice(); // for copy the array
    function loop () {
      let promises = [];
      if (!array.length) {
        return 'finish';
      }
      // loop on numberLoop and push for promise all to make some promises together
      for (var index = 0; ((index < numberLoop) && array.length); index++) {
        let itemArguments = array.shift();
        if (arrayValuesItemsIsNotObject) { // in array values each item is not object
          promises.push(service[functionService](itemArguments));
        } else { // send to func service all arguments - we take him from item properties
          if (!Array.isArray(itemArguments)) {
            itemArguments = [itemArguments];
          }
          promises.push(service[functionService].apply(null, itemArguments));
        }
      }
      console.log('finish ' + numberLoop);
      return Promise.all(promises)
      .then(res => loop(array))
      .catch(ErrorEvent => loop(array));
    }
    return loop();
  }
};
