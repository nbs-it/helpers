/*********************************
 * See exmple at the bottom page
**********************************/

/**
 * query for update multiple rows (bulkUpdate)
 *
 * @param {string} text - includes the table name and the operation
 * @param {object array} values - array of all items we want to update
 * example: items = [ { set: { ... }, where: { ... } }, ... ]
 * @returns {string} string of the complete query
 */
function createQuery (text, values) {
  // create query with one space between words and make it lowercase
  text = text.toLowerCase().replace(/\s\s+/g, ' ').trim();

  let firstWord = text.split(' ')[0].trim();

  // '-multiply-update-' is a new different query operation
  if (firstWord === '-multiply-update-') {
    return queryMultiplyUpdate(text, values);
  }
  let result = {
    text: text,
    values: values
  };
  return result;
}

/**
* make multiply query update text from array rows
*
 * @param {string} text - includes the table name and the operation
 * @param {object array} values - array of all items we want to update
 * example: items = [ { set: { ... }, where: { ... } }, ... ]
 * @returns {string} string of the complete query
*/
function queryMultiplyUpdate (text, values) {
  let query = { text: '' };
  let nameTable = text.replace('-multiply-update-', '').trim();
  // iteration on all array values, each one create his query update
  values.forEach((itemToUpdate) => {
    let wherePart = ' ';
    let indexDependence = 0;

    // build the query
    query.text += ' update ' + nameTable + ' set ';
    query.text += valuesToQuerySet(itemToUpdate.set);
    query.text += ' where ';
    // adding dependencies (query text part 'where in')
    for (let key in itemToUpdate.where) {
      if (indexDependence !== 0) {
        wherePart += ' AND ';
      }
      indexDependence++;
      wherePart += '"' + key + '"' + " = '" + itemToUpdate.where[key] + "'";
    }
    // add wherePart to query
    query.text += wherePart + ';';
  });
  return query;
}

/**
* build values set for one query update
*
* @param {object} valuesForSet - values to set
    example : { prop1: 'value1' , prop2: 'value2 }
* @returns
*/
function valuesToQuerySet (valuesForSet) {
  // add the values to query text
  let setTextQuery = '';
  Object.keys(valuesForSet).forEach((column, indexColumn) => {
    let value = valuesForSet[column];
    let textValue = '';
    value = (value && value.replace) ? value.replace(/'/g, '`') : value;
    if (value === '') {
      value = ' ';
    }
    if ((typeof value === 'string')) {
      textValue += "'" + value;
    } else {
      textValue += ' ' + value;
    }
    if (indexColumn !== Object.keys(valuesForSet).length - 1) {
      textValue += "' , ";
    }
    setTextQuery += ' "' + column + '"' + ' = ' + textValue;
  });
  return setTextQuery;
}

module.exports = createQuery;

/**************
 * Example
 *
let text = '-multiply-update- example';
let values = [
  {
    set: { name: 'tramp', value: 'president' },
    where: { where1Example1: '1233', where1Example2: '1234' }
  },
  {
    set: { name: 'hilery', value: 'loser' },
    where: { where2Example1: '12343', where2Example2: '12344' }
  }
];
console.log(createQuery(text, values));

result --
  { text:'  update example set  "name" = \'tramp\' ,  "value" = \'president
            where "where1Example1" = \'1233\' AND "where1Example2" = \'1234\';

            update example set  "name" = \'hilery\' ,  "value" = \'loser
            where  "where2Example1" = \'12343\' AND "where2Example2" = \'12344\';'
    }

***************/
