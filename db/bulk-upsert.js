// import _ from 'lodash';

module.exports = class globalFunctionsDB {
  static bulkUpsert (connect, model, array) {
    let updates = [];
    let creates = [];

    array.forEach((item) => {
      if (item.id) {
        let id = item.id;
        delete item.id;
        let itemUpdated = {
          set: item,
          where: {
            id: id
          }
        };
        updates.push(itemUpdated);
      } else {
        creates.push(item);
      }
    });
    return model.bulkCreate(creates)
      .then(() => {
        return connect.makeQuery('-multiply-update- "' + model.getTableName() + '"', updates);
      })
      .catch((err) => {
        console.log('bulkUpsert failed in table ' + model.getTableName() + ':', err);
      });
  }
};
